package br.com.buscapico.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "avaliacao")
public class Avaliacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private float nota;
	private LocalDateTime dataAvaliacao;

	@OneToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	public Avaliacao() {

	}

}
