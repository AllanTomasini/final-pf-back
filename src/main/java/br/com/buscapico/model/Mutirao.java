package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.Data;

@Entity
@Data
public class Mutirao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private LocalDateTime dataMutirao;

	private LocalDateTime dataCadastro;
//
//	@OneToMany
//	@JoinColumn(name = "foto_id")
//	private List<Foto> fotos;


	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "criador_id")
	Usuario criador;

	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "participante_id")
	List<Usuario> participantes;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pico_id")
	Pico pico;

	public Mutirao() {

	}

	public Mutirao(LocalDateTime dataMutirao) {
		super();
		this.dataMutirao = dataMutirao;
	}

	public Mutirao(LocalDateTime dataMutirao, Usuario criador, List<Usuario> participantes, Pico pico) {
		super();
		this.dataMutirao = dataMutirao;
		this.criador = criador;
		this.participantes = participantes;
		this.pico = pico;
	}

	public Mutirao(LocalDateTime dataMutirao, Pico pico) {
		super();
		this.dataMutirao = dataMutirao;
		this.pico = pico;
	}

}
