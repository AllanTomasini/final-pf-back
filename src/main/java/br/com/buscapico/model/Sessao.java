package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class Sessao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	@JoinColumn(name = "pico_id")
	private Pico pico;

	private LocalDateTime dataSessao;
	private LocalDateTime dataCadastro;


//	@OneToMany
//	@JoinColumn(name = "comentario_id")
//	private List<Comentario> comentarios;

	
	public Sessao() {}
	
	public Sessao(LocalDateTime dataSessao ,LocalDateTime dataCadastro  ) {
		super();
		
		this.dataSessao = dataSessao;
		this.dataCadastro = dataCadastro;
	}
	
}
