package br.com.buscapico.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "localizacao")
public class Localizacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private double latitude;
	private double longitude;

	public Localizacao() {

	}

	public Localizacao(double latitude, double longitude) {
		this.longitude = longitude;
		this.latitude = latitude;
	}
}
