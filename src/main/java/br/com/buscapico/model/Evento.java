package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Evento {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;

	@OneToMany
	@JoinColumn(name = "etapa_id")
	private List<Etapa> etapas;

	private LocalDateTime dataCadastro;

//	@OneToMany
//	@JoinColumn(name = "foto_id")
//	private List<Foto> fotos;

//
//	@OneToMany
//	@JoinColumn(name = "comentario_id")
//	private List<Comentario> comentarios;

	public Evento() {

	}
	public Evento(String nome, LocalDateTime dataCadastro) {
		super();
		this.nome = nome;
		this.dataCadastro = dataCadastro;
	}

}
