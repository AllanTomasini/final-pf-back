package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.val;

@Data
@Entity
@Table(name = "anuncio")
public class Anuncio {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String titulo;

	private double preco;

	private String descricao;

	private LocalDateTime dataCadastro;

	@OneToMany
	@JoinColumn(name = "foto_id")
	private List<Foto> fotos;

	@OneToMany
	@JoinColumn(name = "comentario_id")
	private List<Comentario> comentarios;

	@OneToOne
	@JoinColumn(name = "anunciante_id")
	private Usuario anunciante;

	@OneToOne
	@JoinColumn(name = "categoria_id")
	private CategoriaAnuncio categoria;

	private Double precoComPercentual;

	@Transient
	private Double percentual;

	public Anuncio() {

	}

	public Anuncio(String titulo, double preco, String descricao, LocalDateTime dataCadastro, Double precoComPercentual,
			Double percentual) {
		super();
		this.titulo = titulo;
		this.preco = preco;
		this.descricao = descricao;
		this.dataCadastro = dataCadastro;
		this.precoComPercentual = precoComPercentual;
		this.percentual = percentual;
	}

}
