package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String nome;
	private String senha;

	private LocalDateTime dataNascimento;

	@OneToMany
	@JoinColumn(name = "contato_id")
	private List<Usuario> contatos;

	@OneToMany
	@JoinColumn(name = "favorito_id")
	private List<Favorito> favoritos;

	public Usuario() {

	}

	public Usuario(String nome) {
		this.nome = nome;
	}

	public Usuario(String nome, String senha) {
		this.nome = nome;
		this.senha = senha;
	}
}
