package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Data;

@Data
@Entity
@Table(name = "pico")
public class Pico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;
	private String descricao;
	private double entrada;
	private LocalDateTime dataCriacao;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "endereco_id")
	private Endereco endereco;

	@OneToMany
	@JoinColumn(name = "tipo_id")
	private List<Tipo> tipos;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "comentario_id")
	@Fetch(FetchMode.SUBSELECT)
	private List<Comentario> comentarios;

	@OneToMany
	@JoinColumn(name = "denuncia_id")
	private List<Denuncia> denuncias;

	private String urlCapa;
	@OneToMany
	@JoinColumn(name = "checkin_id")
	private List<CheckIn> checkIns;

	@OneToMany
	@JoinColumn(name = "avaliacao_id")
	private List<Avaliacao> avaliacoes;

	@OneToMany
	@JoinColumn(name = "video_id")
	private List<Video> videos;

	@OneToMany
	@JoinColumn(name = "foto_id")
	private List<Foto> fotos;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "criador_id")
	private Usuario criador;

	private boolean favorito;

	public Pico() {
	}

	public Pico(String nome, String descricao, double entrada, LocalDateTime dataCriacao) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.entrada = entrada;
		this.dataCriacao = dataCriacao;
	}

	public Pico(String nome, String descricao, double entrada, LocalDateTime dataCriacao, Endereco endereco,
			String urlCapa, boolean favorito, List<Comentario> comentarios, Usuario criador) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.entrada = entrada;
		this.dataCriacao = dataCriacao;
		this.endereco = endereco;
		this.urlCapa = urlCapa;
		this.favorito = favorito;
		this.comentarios = comentarios;
		this.criador = criador;
	}

}
