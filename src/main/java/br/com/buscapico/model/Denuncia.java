package br.com.buscapico.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name = "denuncia")
public class Denuncia {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private LocalDateTime data;
	private String status;
	private String descricao;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "pico_id")
	private Pico pico;

	@Transient
	private Long idPico;

	public Denuncia() {

	}
}
