package br.com.buscapico.model;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Feed {
	private String tipo;
	private String descricao;
	private Usuario usuario;
	private LocalDateTime data;
	private Long id;

	public Feed(String tipo, String descricao, Usuario usuario, LocalDateTime data, Long id) {
		this.tipo = tipo;
		this.descricao = descricao;
		this.usuario = usuario;
		this.data = data;
		this.id = id;
	}

}
