package br.com.buscapico;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Anuncio;
import br.com.buscapico.repository.AnuncioRepository;

@Component
@Order(value = 9)
public class AnuncioRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(AnuncioRunner.class);

	@Autowired
	private AnuncioRepository repository;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Anúncio");

		repository
				.save(new Anuncio("rodinha de skate", 100.00, "4 rodinhas spitfire", LocalDateTime.now(), 120.00, 0.2));
		repository
				.save(new Anuncio("rodinha de skate", 100.00, "4 rodinhas spitfire", LocalDateTime.now(), 120.00, 0.2));
		repository
				.save(new Anuncio("rodinha de skate", 100.00, "4 rodinhas spitfire", LocalDateTime.now(), 120.00, 0.2));
		repository
				.save(new Anuncio("rodinha de skate", 100.00, "4 rodinhas spitfire", LocalDateTime.now(), 120.00, 0.2));
		repository
				.save(new Anuncio("rodinha de skate", 100.00, "4 rodinhas spitfire", LocalDateTime.now(), 120.00, 0.2));
		repository
				.save(new Anuncio("rodinha de skate", 100.00, "4 rodinhas spitfire", LocalDateTime.now(), 120.00, 0.2));

		log.info("Finalizando");
	}
}
