package br.com.buscapico;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Sessao;
import br.com.buscapico.repository.SessaoRepository;


@Component
@Order(value = 9)
public class SessaoRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(SessaoRunner.class);
	
	@Autowired
	private SessaoRepository repository;
	
	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de sessões");
		
//		Pico pico = new Pico("Jardim Ambiental", "Pico clássico com transições.", 0d, LocalDateTime.now());

		
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
		repository.save(new Sessao( LocalDateTime.now(), LocalDateTime.now())) ;
			
		log.info("Finalizando");
	}
}
