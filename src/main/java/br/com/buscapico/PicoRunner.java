package br.com.buscapico;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Endereco;
import br.com.buscapico.model.Localizacao;
import br.com.buscapico.model.Pico;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.PicoRepository;

@Component
@Order(value = 8)
public class PicoRunner implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(PicoRunner.class);

	@Autowired
	private PicoRepository repository;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de picos");
		List<Endereco> enderecos = new ArrayList<>();

		enderecos.add(new Endereco("R. Schiller", 0, new Localizacao(-25.423167, -49.249763)));
		enderecos.add(new Endereco("Praça Afonso Botelho", 0, new Localizacao(-25.446377, -49.274522)));
		enderecos.add(new Endereco("Praça do Redentor", 0, new Localizacao(-25.420891, -49.274805)));
		
		Comentario a =  new Comentario("Pista bem conservada, muito movimentada nos finais de semana",LocalDateTime.now(), new Usuario("João Silva"));
		Comentario b = new Comentario("Falta segurança",LocalDateTime.now(), new Usuario("Caju Manoel"));
		Comentario c = new Comentario("Gostei",LocalDateTime.now(), new Usuario("Pedro Henrique"));
		Comentario d =  new Comentario("Precisa de reformas",LocalDateTime.now(), new Usuario("Paulo França"));
		Comentario e = new Comentario("Perigosa à noite",LocalDateTime.now(), new Usuario("Jeferson Luis"));

		List<Comentario> comentariosAmbiental = new ArrayList<Comentario>();
		comentariosAmbiental.add(d);
		comentariosAmbiental.add(e);
		
		
		List<Comentario> comentariosAtletico = new ArrayList<Comentario>();
		comentariosAtletico.add(b);
		comentariosAtletico.add(c);
		List<Comentario> comentariosGaucho = new ArrayList<Comentario>();
		comentariosGaucho.add(a);

		repository.save(new Pico("Jardim Ambiental", "Pico possui transições rápidas. Uma das pistas mais antigas de Curitiba", 0d, LocalDateTime.now(),
				enderecos.get(0), "ambiental.jpg", true, comentariosAmbiental, new Usuario("Neto")));
		repository.save(new Pico("Pista do Gaúcho", "Pico com transições suaves. Pista antiga reformada recentemente.", 0d, LocalDateTime.now(),
				enderecos.get(2), "gaucho.jpeg", false, comentariosGaucho, new Usuario("Rafael")));
		repository.save(new Pico("Pista do Atlético", "Pista de street. Reformada recentemente", 0d, LocalDateTime.now(),
				enderecos.get(1), "atletico.jpeg", true, comentariosAtletico, new Usuario("Lucas")));

		log.info("Finalizando");
	}
}
