package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Foto;

@Repository
public interface FotoRepository extends CrudRepository<Foto, Long> {

}
