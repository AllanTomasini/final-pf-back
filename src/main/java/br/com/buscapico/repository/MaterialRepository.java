package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Material;

@Repository
public interface MaterialRepository extends CrudRepository<Material, Long> {

}
