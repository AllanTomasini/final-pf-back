package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Etapa;

@Repository
public interface EtapaRepository extends CrudRepository<Etapa, Long>{

}
