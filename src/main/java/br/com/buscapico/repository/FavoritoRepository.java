package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Favorito;

@Repository
public interface FavoritoRepository extends CrudRepository<Favorito, Long>{

}
