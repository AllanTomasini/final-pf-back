package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.CategoriaAnuncio;

@Repository
public interface CategoriaAnuncioRepository extends CrudRepository<CategoriaAnuncio, Long> {

}
