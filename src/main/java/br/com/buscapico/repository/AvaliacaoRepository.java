package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Avaliacao;

@Repository
public interface AvaliacaoRepository extends CrudRepository<Avaliacao, Long>{

}
