package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Localizacao;

@Repository
public interface LocalizacaoRepository extends CrudRepository<Localizacao, Long> {

}
