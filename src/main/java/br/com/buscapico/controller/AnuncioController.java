package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Anuncio;
import br.com.buscapico.model.Sessao;
import br.com.buscapico.repository.AnuncioRepository;

@RestController
@RequestMapping("anuncios")
public class AnuncioController {

	private static final Logger log = LoggerFactory.getLogger(AnuncioController.class);

	@Autowired
	private AnuncioRepository repository;

	
	@GetMapping("/listar")
	public List<Anuncio> listarAnuncios() {
		log.info("listarAnuncios");

		return (List<Anuncio>) repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Anuncio> detalheAnuncio(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
	
	@DeleteMapping("/{id}/excluir")
	public void deletarAnuncio(@PathVariable String id) {
		repository.deleteById(Long.parseLong(id));

	}
	
	@PutMapping("/{id}/editar")
	public void editarAnuncio(@PathVariable String id ,@RequestBody Anuncio body) {
		Optional<Anuncio> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Anuncio anuncio = c.get();
			anuncio.setDescricao(body.getDescricao());
			anuncio.setPreco(body.getPreco());
			anuncio.setTitulo(body.getTitulo());
			repository.save(anuncio);
		}
	}
	
	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarAnuncio(@RequestBody Anuncio body) {
		try {
			body.setPrecoComPercentual(body.getPreco() * (1 + (body.getPercentual()/100)));
			repository.save(body);

			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

