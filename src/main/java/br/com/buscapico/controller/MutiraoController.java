package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Mutirao;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.MutiraoRepository;
import br.com.buscapico.repository.UsuarioRepository;

@RestController
@RequestMapping("mutiroes")

public class MutiraoController {

	private static final Logger log = LoggerFactory.getLogger(MutiraoController.class);

	@Autowired
	private MutiraoRepository repository;

	@Autowired
	private UsuarioRepository userRep;

	@GetMapping("/listar")
	public List<Mutirao> listarPicos() {
		log.info("listarMutiroes");

		return (List<Mutirao>) repository.findAll();
	}

	@GetMapping("/listarUsuarios")
	public List<Usuario> listarUsuarios() {
		log.info("listarUsuarios");

		return (List<Usuario>) userRep.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Mutirao> detalheMutirao(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}

	@GetMapping("/{id}/sair")
	public void sair(@PathVariable String id) {
		Mutirao m = repository.findById(Long.parseLong(id)).get();

		m.getParticipantes().removeIf(u -> u.getNome().contains("Allan"));

		repository.save(m);

	}

	@GetMapping("/{id}/participar")
	public void participar(@PathVariable String id) {
		Mutirao m = repository.findById(Long.parseLong(id)).get();
		userRep.findAll().forEach(u -> {
			if (u.getNome().contains("Allan")) {

				m.getParticipantes().add(u);
			}
		});

		repository.save(m);

	}

	@DeleteMapping("/{id}/excluir")
	public void deletarMutirao(@PathVariable String id) {
		repository.deleteById(Long.parseLong(id));

	}

	@PutMapping("/{id}/editar")
	public void editarAnuncio(@PathVariable String id, @RequestBody Mutirao body) {
		Optional<Mutirao> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Mutirao mutirao = c.get();
			mutirao.setDataMutirao(body.getDataMutirao());
			repository.save(mutirao);
		}
	}

	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public void cadastrarMutirao(@RequestBody Mutirao body) {

		userRep.findAll().forEach(u -> {
			if (u.getNome().contains("Allan")) {
				body.setCriador(u);
			}
		});

		body.setParticipantes(body.getParticipantes().stream().map(part -> {
			return userRep.findById(part.getId()).get();
		}).collect(Collectors.toList()));

		repository.save(body);

	}

}
