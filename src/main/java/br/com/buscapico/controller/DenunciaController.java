package br.com.buscapico.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Denuncia;
import br.com.buscapico.model.Pico;
import br.com.buscapico.repository.DenunciaRepository;
import br.com.buscapico.repository.PicoRepository;

@RestController
@RequestMapping("denuncias")
public class DenunciaController {
	private static final Logger log = LoggerFactory.getLogger(DenunciaController.class);

	@Autowired
	private DenunciaRepository denunciaRep;

	@Autowired
	private PicoRepository picoRep;

	@GetMapping("/listar")
	public List<Denuncia> listarPicos() {
		log.info("listar Denuncias");

		List<Denuncia> denuncias = new ArrayList<Denuncia>();

		denunciaRep.findAll().forEach(denuncia -> denuncias.add(denuncia));

		return denuncias;
	}

	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> inserirDenuncia(@RequestBody Denuncia body) {
		try {
			log.info("cadastrar Denuncia" + body);
			Pico p = picoRep.findById(body.getIdPico()).get();

			body.setPico(p);

			denunciaRep.save(body);
			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/rejeitar", consumes = "application/json", produces = "application/json")
	public void rejeitarDenuncia(@RequestBody Denuncia body) {
		try {
			log.info("rejeitar Denuncia" + body);

			body.setStatus("Rejeitada");
			denunciaRep.save(body);

		} catch (Exception ex) {
		}
	}

	@PostMapping(value = "/aceitar", consumes = "application/json", produces = "application/json")
	public void aceitarDenuncia(@RequestBody Denuncia body) {
		try {
			log.info("aceitar Denuncia" + body);
			Pico p = body.getPico();
			body.setPico(null);
			body.setStatus("Aceita");
			denunciaRep.save(body);
			picoRep.delete(p);
			

		} catch (Exception ex) {
		}
	}

}
