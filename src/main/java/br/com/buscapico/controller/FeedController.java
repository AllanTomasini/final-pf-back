package br.com.buscapico.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Feed;
import br.com.buscapico.model.Mutirao;
import br.com.buscapico.model.Pico;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.MutiraoRepository;
import br.com.buscapico.repository.PicoRepository;

@RestController
@RequestMapping("feed")
public class FeedController {
	private static final Logger log = LoggerFactory.getLogger(FeedController.class);

	@Autowired
	private PicoRepository picoRep;
	@Autowired
	private MutiraoRepository mutiRep;

	@GetMapping("/listar")
	public List<Feed> listarFeed() {
		log.info("listarPicos");

		List<Feed> feed = new ArrayList<Feed>();

		picoRep.findAll().forEach(pico -> feed.add(getFeedFromPico(pico)));
		mutiRep.findAll().forEach(mutirao -> feed.add(getFeedFromMutirao(mutirao)));

		return feed.stream().filter(f -> !f.getUsuario().getNome().contains("Allan")).sorted(Comparator.nullsFirst((f1, f2) -> f1.getData().compareTo(f2.getData())))
				.collect(Collectors.toList());
	}

	private Feed getFeedFromPico(Pico pico) {
		return new Feed("Pico", "Pico: "+ pico.getNome(),
				pico.getCriador() == null ? new Usuario("Allan Tomasini") : pico.getCriador(),
				pico.getDataCriacao() == null ? LocalDateTime.now() : pico.getDataCriacao(), pico.getId());
	}

	private Feed getFeedFromMutirao(Mutirao mutirao) {
		return new Feed("Mutirão", "Mutirão: " + mutirao.getPico().getNome(), mutirao.getCriador(),
				mutirao.getDataCadastro() == null ? LocalDateTime.now() : mutirao.getDataCadastro(), mutirao.getId());
	}

}
