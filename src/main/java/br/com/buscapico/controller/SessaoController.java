package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Sessao;
import br.com.buscapico.repository.SessaoRepository;
@RestController
@RequestMapping("sessaos")
public class SessaoController {
	private static final Logger log = LoggerFactory.getLogger(SessaoController.class);

	@Autowired
	private SessaoRepository repository;

	@GetMapping("/listar")
	public List<Sessao> listarSessaos() {
		log.info("listarSessoes");

		return (List<Sessao>) repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Sessao> detalheSessao(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
	
	@DeleteMapping("/{id}/excluir")
	public void deletarSessao(@PathVariable String id) {
		repository.deleteById(Long.parseLong(id));

	}
	
	@PutMapping("/{id}/editar")
	public void editarSessao(@PathVariable String id ,@RequestBody Sessao body) {
		Optional<Sessao> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Sessao sessao = c.get();
			sessao.setDataSessao(body.getDataSessao());
			
			repository.save(sessao);
		}
	}
	
	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarSessao(@RequestBody Sessao body) {
		try {
			repository.save(body);

			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
