package br.com.buscapico.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Foto;
import br.com.buscapico.model.Pico;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.model.Video;
import br.com.buscapico.repository.ComentarioRepository;
import br.com.buscapico.repository.FotoRepository;
import br.com.buscapico.repository.PicoRepository;
import br.com.buscapico.repository.UsuarioRepository;
import br.com.buscapico.repository.VideoRepository;

@RestController
@RequestMapping("picos")
public class PicoController {
	private static final Logger log = LoggerFactory.getLogger(PicoController.class);

	@Autowired
	private PicoRepository repository;

	@Autowired
	private ComentarioRepository comentarioRepository;

	@Autowired
	private VideoRepository vidRep;

	@Autowired
	private UsuarioRepository userRep;
	
	@Autowired
	private FotoRepository fotoRep;

	@GetMapping("/listar")
	public List<Pico> listarPicos() {
		log.info("listarPicos");

		List<Pico> picos = new ArrayList<Pico>();

		repository.findAll().forEach(pico -> picos.add(pico));

		return picos;
	}

	@GetMapping("/listarFavoritos")
	public List<Pico> listarPicosFavoritos() {
		log.info("listarPicos");

		List<Pico> picos = new ArrayList<Pico>();

		repository.findAll().forEach(pico -> picos.add(pico));

		return picos.stream().filter(pico -> pico.isFavorito()).collect(Collectors.toList());
	}

	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarPico(@RequestBody Pico body) {
		try {
			log.info("cadastrarPico" + body);
			repository.save(body);
			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/inserirVideo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarVideo(@RequestBody Video body) {
		try {
			log.info("cadastrarvideos" + body);
			Pico p = repository.findById(body.getIdPico()).get();

			userRep.findAll().forEach(usuario -> {
				if (usuario.getNome().contains("Allan")) {
					body.setUsuario(usuario);
				}
			});
			vidRep.save(body);

			p.getVideos().add(body);

			repository.save(p);
			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping(value = "/inserirFoto", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarFoto(@RequestBody Foto body) {
		try {
			log.info("cadastrar Fotos" + body);
			Pico p = repository.findById(body.getIdPico()).get();

			userRep.findAll().forEach(usuario -> {
				if (usuario.getNome().contains("Allan")) {
					body.setUsuario(usuario);
				}
			});
			fotoRep.save(body);

			p.getFotos().add(body);

			repository.save(p);
			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{id}")
	public Optional<Pico> detalhePico(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}

	@GetMapping("/{id}/fotos")
	public List<Foto> listarFotos(@PathVariable String id) {
		Optional<Pico> pico = repository.findById(Long.parseLong(id));
		return pico.map(Pico::getFotos).orElse(null);
	}

	@PostMapping(value = "/{id}/comentar", consumes = "application/json", produces = "application/json")
	public void comentar(@PathVariable String id, @RequestBody Map<String, String> json) {

		Optional<Pico> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Pico pico = c.get();
			pico.getComentarios()
					.add(new Comentario(json.get("comentario"), LocalDateTime.now(), new Usuario("Allan Tomasini")));
			repository.save(pico);
		}

	}

	@DeleteMapping("/{id}/comentario/{idComentario}")
	public void deletarComentario(@PathVariable String idComentario) {
		comentarioRepository.deleteById(Long.parseLong(idComentario));

	}

	@PutMapping("/{id}/comentario/{idComentario}/editar")
	public void editarComentario(@PathVariable String idComentario, @RequestBody String texto) {
		Optional<Comentario> c = comentarioRepository.findById(Long.parseLong(idComentario));

		if (c.isPresent()) {
			Comentario comentario = c.get();
			comentario.setTexto(texto);
			comentarioRepository.save(comentario);
		}
	}

	@GetMapping("/{id}/editarFavorito")
	public void removerFavorito(@PathVariable String id) {
		Optional<Pico> pico = repository.findById(Long.parseLong(id));
		if (pico.isPresent()) {
			Pico p = pico.get();
			p.setFavorito(!p.isFavorito());
			repository.save(p);
		}
	}
}
