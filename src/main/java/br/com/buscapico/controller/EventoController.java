package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Evento;
import br.com.buscapico.repository.EventoRepository;

@RestController
@RequestMapping("eventos")
public class EventoController {
	private static final Logger log = LoggerFactory.getLogger(EventoController.class);

	@Autowired
	private EventoRepository repository;

	@GetMapping("/listar")
	public List<Evento> listarEventos() {
		log.info("listarEventos");

		return (List<Evento>) repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Evento> detalheEvento(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
	
	
	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarEvento(@RequestBody Evento body) {
		try {
			repository.save(body);

			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
