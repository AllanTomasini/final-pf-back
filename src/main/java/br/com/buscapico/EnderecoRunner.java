package br.com.buscapico;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Endereco;
import br.com.buscapico.model.Localizacao;
import br.com.buscapico.repository.EnderecoRepository;
import br.com.buscapico.repository.LocalizacaoRepository;

@Component
@Order(value = 2)
public class EnderecoRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(EnderecoRunner.class);

	@Autowired
	private EnderecoRepository repository;

	@Autowired
	private LocalizacaoRepository localizacaoRepository;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Endereços");

		List<Endereco> enderecos = new ArrayList<>();
//		List<Localizacao> localizacoes = new ArrayList<>();
//
//		localizacaoRepository.findAll().forEach(l -> localizacoes.add(l));

		enderecos.add(new Endereco("R. Schiller", 0, new Localizacao(-25.423167, -49.249763)));
		enderecos.add(new Endereco("Praça do Redentor", 0, new Localizacao(-25.446377, -49.274522)));
		enderecos.add(new Endereco("Praça Afonso Botelho", 0, new Localizacao(-25.420891, -49.274805)));

		repository.saveAll(enderecos);

		log.info("Finalizando");
	}

}
