package br.com.buscapico;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Mutirao;
import br.com.buscapico.model.Pico;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.MutiraoRepository;
import br.com.buscapico.repository.PicoRepository;
import br.com.buscapico.repository.UsuarioRepository;

@Component
@Order(value = 9)
public class MutiraoRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(MutiraoRunner.class);

	@Autowired
	private MutiraoRepository repository;

	@Autowired
	private PicoRepository picoRep;

	@Autowired
	private UsuarioRepository usuarioRep;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Anúncio");

		List<Pico> picos = new ArrayList<Pico>();

		picoRep.findAll().forEach(p -> picos.add(p));

		Usuario u1 = new Usuario("Marcos Vinicius");
		Usuario u2 = new Usuario("Raul Seixas");
		Usuario u3 = new Usuario("Allan Tomasini");

		usuarioRep.save(u1);
		usuarioRep.save(u2);
		usuarioRep.save(u3);

		List<Usuario> lu1 = new ArrayList<>();
		List<Usuario> lu2 = new ArrayList<>();

		lu1.add(u1);

		lu2.add(u2);
		lu2.add(u3);

		LocalDateTime d1 = LocalDateTime.of(2018, 12, 8, 12, 0);
		LocalDateTime d2 = LocalDateTime.of(2018, 12, 20, 12, 0);

		Mutirao m1 = new Mutirao(d1, picos.get(0));
		Mutirao m2 = new Mutirao(d2, picos.get(1));

		repository.save(m1);
		repository.save(m2);

		m1.setCriador(u1);
		m2.setCriador(u3);
		m1.setParticipantes(new ArrayList<>());
		m2.setParticipantes(new ArrayList<>());
		m1.getParticipantes().addAll(lu2);
		m2.getParticipantes().addAll(lu1);
		
		repository.save(m1);
		repository.save(m2);

		log.info("Finalizando");
	}
}
