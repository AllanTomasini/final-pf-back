package br.com.buscapico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Localizacao;
import br.com.buscapico.repository.LocalizacaoRepository;

@Component
@Order(value = 1)
public class LocalizacaoRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(EventoRunner.class);

	@Autowired
	private LocalizacaoRepository repository;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Evento");

//		// ambiental
//		repository.save(new Localizacao(-25.423167, -49.249763));
//		// atletico
//		repository.save(new Localizacao(-25.446377, -49.274522));
//		// gaúcho
//		repository.save(new Localizacao(-25.420891, -49.274805));

		log.info("Finalizando");
	}
}